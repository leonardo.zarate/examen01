﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed=0.5f;
    public Sprite[] objects;
    public int rand;

    
    // Start is called before the first frame update
    void Start()
    {
        rand = Random.Range(0, objects.Length);
        GetComponent<SpriteRenderer>().sprite = objects[rand];
        
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.x == 0 && this.transform.position.y > 0.4f)
        {
            this.transform.Translate(0, -(speed * Time.deltaTime), 0);
        }
        if (transform.position.y <= -10)
        {
            Destroy(this.gameObject);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player" || collision.gameObject.tag == "Collision")
        {
            Destroy(this.gameObject);
        }
    }
}
