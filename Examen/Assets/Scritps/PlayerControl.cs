﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour
{
    public float speed;
    public float dir;
    public Animator anim;
    public Collider2D coll;
    public Rigidbody2D rig;
    private bool moveLeft, moveRight;
    private int lives = 3;
    private int scorenum;
    public Text scores;
    public Text Lives;
    public Enemy enemy;
    
    
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        coll = GetComponent<Collider2D>();
        moveLeft = false;
        moveRight = false;
        rig = GetComponent<Rigidbody2D>();
        scorenum = 0;
        scores.text = "Score: " + scorenum;
        Lives.text = "Lives: " + lives;
        enemy = new Enemy();
        
    }

    // Update is called once per frame
    void Update()
    {
       if(moveLeft)
        {
            rig.velocity = new Vector2(-speed, 0f);
        }
        if (moveRight)
        {
            rig.velocity = new Vector2(speed, 0f);
        }
    }
    public void MoveL()
    {
        moveLeft = true;
        transform.localScale = new Vector3(-1, 1, 1);
        anim.SetBool("IsRunning", true);
    }
    public void MoveR()
    {
        moveRight = true;
        transform.localScale = new Vector3(1, 1, 1);
        anim.SetBool("IsRunning", true);
    }
    public void StopMov()
    {
        moveLeft = false;
        moveRight = false;
        rig.velocity = Vector2.zero;
        anim.SetBool("IsRunning", false);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Enemies" || enemy.rand ==5)
        {
            scorenum += 10;
            scores.text = "Score: " + scorenum;
            Destroy(enemy);
        }
        if (collision.tag == "Enemies" || enemy.rand < 5)
        {
            lives--;
            Lives.text = "Lives: " + lives;
            Destroy(enemy);
        }
    }
}
